import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'
import numpy as np
from fractions import Fraction


class assetBasket:
#a_w : asset and weigh pandas dataframe
#nulPickWeight : is the nul pick weight column part of the data ?
#weightsComputedAtEOD : are the weights computed using data collected at end of the day ...
# ... if so we can onlh invest on this info on the next day
	def __init__(self, a_w, nulPickWeight=True, weightsComputedAtEOD=True):
		#First we assign assets and weights to properties
		self.asset = a_w[a_w.columns[:len(a_w.columns)//2]]
		self.weight =  a_w[a_w.columns[len(a_w.columns)//2:]]
		#Then we check if we need to create NULpick columns for asset or asset and weight
		self.__checkEntriesForNULpick()
		#If the information to compute the wheights is end of day we invest the next day so need to shift the weights
		if weightsComputedAtEOD:
			self.weight = self.weight.shift(1).fillna(0)
		#Useful properties
		self.nasset = len(self.asset.columns)
		self.ndates = len(self.asset)
#Will check in the entries if we have a NULpick and will add one if not
	def __checkEntriesForNULpick(self):
		#If even number of asset and weights we have two possibilities
		if len(self.asset.columns) == len(self.weight.columns):
			#Either there is no null pick asset nor weigth for it
			if nulPickWeight == False:
				#So we create the null pick weight and asset
				self.asset['NUL'] = 1
				self.weight['NULPick'] = 0
			#Or we already have both so we pass
			else:
				pass
		#If uneven number of asset and weight we suppose we have a null pick weight but no null pick asset
		else:
			#Then we create the null pick asset column
			self.asset['NUL'] = 1
#Will add a leverage cost rate time series, make sure it is a pandas with same dates as self.asset
	def add_leverage_cost(self, leverage_cost=None):
		if leverage_cost is None:
			self.leverage_cost = pd.DataFrame(np.zeros(len(self.asset)), index=self.asset.index)
		else:
			if len(leverage_cost) != len(self.asset):
				raise ValueError('The leverage_cost time series must be the size of the asset time series')
			self.leverage_cost = leverage_cost
		return self.leverage_cost
#Will check the arguments and make sure a backtest can be run
	def __checkArgumentsForBacktest(self,
		rebalancePeriod, rebalanceProportion, holdingPeriod, 
		multiAssetPick, leverage,
		precisionExponent, startingValue):
		#We determine the remaining parameters according to given parameters
		#Note that rebalanceProportion = rebalancePeriod / holdingPeriod
		#1) if no parameter: we assume a daily rebablance of the whole portfolio
		if  rebalancePeriod is None and rebalanceProportion is None and holdingPeriod is None:
			rebalancePeriod = int(1)
			rebalanceProportion = float(1)
			holdingPeriod = int(1)
		#2) if one parameter:
		#if only holdingPeriod given: we assume a daily rebalance so the proportion is given
		elif rebalancePeriod is None and rebalanceProportion is None:
			rebalancePeriod = int(1)
			rebalanceProportion = float(1/holdingPeriod)
		#if only rebalancePeriod given: we assume a full rebalance every time so the holding period is given
		elif rebalanceProportion is None and holdingPeriod is None:
			rebalanceProportion = float(1)
			holdingPeriod = int(rebalancePeriod)
		#if only rebalanceProportion is given: we find integer numbers that correspond to its ratio and assign them
		#The lowest goes for the rebalancePeriod and the biggest goes for the holdingPeriod
		elif rebalancePeriod is None and holdingPeriod is None:
			rfrac = Fraction.from_float(rebalanceProportion).limit_denominator()
			rebalancePeriod =int(min(rfrac.denominator, rfrac.numerator))
			holdingPeriod = int(max(rfrac.denominator, rfrac.numerator))
		#3) if two parameters:
		elif rebalancePeriod is None:
			rebalancePeriod = int(rebalanceProportion * holdingPeriod)
		elif rebalanceProportion is None:
			rebalanceProportion = float(rebalancePeriod / holdingPeriod)
		elif holdingPeriod is None:
			holdingPeriod = int(rebalancePeriod / rebalanceProportion)
		#4) if 3 parameters
		if rebalanceProportion != rebalancePeriod / holdingPeriod:
				raise ValueError('It is mandatory that rebalanceProportion = rebalancePeriod / holdingPeriod')
		#5) We check the parameters for impossible values
		if isinstance(rebalancePeriod, int) == False or rebalancePeriod < 0:
			raise ValueError('rebalancePeriod must be a positive integer')
		if rebalanceProportion <= 0 or rebalanceProportion > 1:
			raise ValueError('rebalanceProportion must be strictly supperior to zero and inferior to one')
		if isinstance(holdingPeriod, int) == False or holdingPeriod < 1:
			raise ValueError('holdingPeriod must be a positive integer')
		#6)We then assign arguments to private properties
		self.__rebalancePeriod = rebalancePeriod
		self.__rebalanceProportion = rebalanceProportion
		self.__holdingPeriod = holdingPeriod
		self.__multiAssetPick = multiAssetPick
		self.__leverage = leverage
		self.__precisionExponent = precisionExponent
		self.__startingValue = startingValue
		#7)We  create a dictionnary to return and assign to public property
		self.parameters = {
			'rebalancePeriod' : rebalancePeriod, 'rebalanceProportion' :rebalanceProportion , 'holdingPeriod' : holdingPeriod,
			'multiAssetPick' : multiAssetPick, 'leverage' : leverage, 
			'precisionExponent' : precisionExponent, 'startingValue' : startingValue}
#Will initialize the dynamic portfolio
	def __init_dyn_portfolio(self):
		portfolio = np.zeros((self.__holdingPeriod, len(self.weight.columns)))
		portfolio[:,len(self.weight.columns)-1] = 10**self.__precisionExponent / self.__holdingPeriod
		portfolio = pd.DataFrame(portfolio, columns=self.weight.columns)
		self.__dyn_portfolio = portfolio
#Will compute the daily portfolio by summing all the sub portfolios
	def __dailyPortfolio(self):
		dayPortfolio = np.sum(self.__dyn_portfolio, axis=0) / (10**self.__precisionExponent)
		return dayPortfolio
#Will initialize the historic portfolio:
	def __init_hist_portfolio(self):
		historical_portfolio = pd.DataFrame(
											np.zeros((self.ndates, self.nasset)), 
											index=self.weight.index, 
											columns=self.weight.columns
											)
		historical_portfolio.iloc[0,:] = self.__dailyPortfolio()
		self.historical_portfolio = historical_portfolio
#Will update the dynamic portfolio by adding the weights of day n
	def __update_dyn_portfolio(self, n):
		self.__dyn_portfolio = self.__dyn_portfolio.shift(-self.__rebalancePeriod)
		if self.__multiAssetPick:
			self.__dyn_portfolio.iloc[-self.__rebalancePeriod,:] = self.weight.iloc[n,:] * (10**self.__precisionExponent/ self.__holdingPeriod)
		else:
			new_weights = np.zeros(self.nasset)
			new_weights[self.weight.iloc[n,:].argmax()] = 1
			self.__dyn_portfolio.iloc[-self.__rebalancePeriod,:] = new_weights * (10**self.__precisionExponent/ self.__holdingPeriod)
		self.__dyn_portfolio = self.__dyn_portfolio.fillna(method='ffill')
#Will update the historic portfolio by adding the aggregate of the current dynamic portfolio into the row n
	def __update_hist_portfolio(self,n):
		self.historical_portfolio.iloc[n,:] = self.__dailyPortfolio()
#Will compute all dynamic portfolios:
	def __compute_portfolios(self):
		dynamic_portfolios = [self.__dyn_portfolio/ (10**self.__precisionExponent)]
		#We go through every proposed weights
		for i in range(1, self.ndates):
			#But only use the weights that are proposed on days of rebalancing
			#Because we start investing on the second day (index 1), we make sure the portolio is altered on that day
			if (i-1) // self.__rebalancePeriod ==  (i-1) / self.__rebalancePeriod:
				self.__update_dyn_portfolio(i)
			#dynamic portfolios do not change as long as there is no rebalancing, nor do the historic portfolio
			dynamic_portfolios.append(self.__dyn_portfolio/ (10**self.__precisionExponent))
			self.__update_hist_portfolio(i)
		#dynamic_portfolios = np.array(list(map(lambda x: x.to_numpy(), dynamic_portfolios)))
		dynamic_portfolios = pd.concat(dynamic_portfolios, keys=list(self.weight.index), axis=0)
		dynamic_portfolios.index.set_names(['Dates', 'SubPortfolio'], inplace=True)
		self.dynamic_portfolios = dynamic_portfolios
	def __compute_backtest_daily_return(self):
		self.__asset_daily_return = self.asset.pct_change(1).fillna(0)
		backtest_return = self.__asset_daily_return.iloc[:,0] * self.historical_portfolio.iloc[:,0]
		for i in range(1,self.nasset):
			backtest_return = backtest_return + self.__asset_daily_return.iloc[:,i] * self.historical_portfolio.iloc[:,i]
		backtest_return = backtest_return.rename('daily_return')
		if self.__leverage != 1:
			if not hasattr(self, 'leverage_cost'):
				self.add_leverage_cost()
			backtest_return =  self.__leverage * (backtest_return - self.leverage_cost.iloc[:,0])
		self.daily_return = backtest_return
#Will compute the backtest index using the historic portfolio
	def __compute_backtest_value(self):
		backtest_series = pd.DataFrame(np.ones(self.ndates) * self.__startingValue, index=self.weight.index, columns=['backtest'])
		for i in range(1, self.ndates):
			backtest_series.iloc[i] = backtest_series.iloc[i-1] * (1 + self.daily_return.iloc[i])
		self.series = backtest_series
#Will perform a backtest based on the passed parameters
	def backtest(self, 
		rebalancePeriod=None, rebalanceProportion=None, holdingPeriod=None, 
		multiAssetPick=False, leverage=1,
		precisionExponent=6, startingValue=100):
		#1)We prepare the arguments
		self.__checkArgumentsForBacktest(
			rebalancePeriod, rebalanceProportion, holdingPeriod, 
			multiAssetPick, leverage,
			precisionExponent, startingValue)
		#2)a)We initialize a dynamic portfolio containing each subportfolio, the  oldest one being replaced every rebalancePeriod
		self.__init_dyn_portfolio()
		#2)b)We also initialize an aggregate of the dynamic portoflio to see the daily portfolio evolution
		self.__init_hist_portfolio()
		#3) We compute all the dynamic portfolios and use them to compute historic portfolios
		self.__compute_portfolios()
		#4) We generate the backtest daily return series using the historic portfolio
		self.__compute_backtest_daily_return()
		#5) Finally we can produce a series for the backtest
		self.__compute_backtest_value()
		return self.series
#Will return the dynamic portfolio from the dynamic portfolio multiindex array
#Can be called by index number or index value with the index of historical portfolio
	def dynamic_portfolio(self, n):
		if not hasattr(self, 'dynamic_portfolios'):
			raise AttributeError('You first need to perform the backtest with .backtest to use this method')
		#If called with a date as a string
		if isinstance(n, str):
			date = n
		#If not assume is called with an index number referring to the historical portfolio df index
		else:
			date = self.dynamic_portfolios.index[n*self.__holdingPeriod][0]
		dynamic_portfolio_n = self.dynamic_portfolios.loc[date]
		dynamic_portfolio_n.index.set_names(['SubPortfolio {}'.format(date)], inplace=True)
		return dynamic_portfolio_n
	def save(self, name=None, path=''):
		if not hasattr(self, 'series'):
			raise AttributeError('You first need to perform the backtest with .backtest to use this method')
		if name is None:
			name = 'backtest_rebalancePeriod{rp1:.0f}d-rebalanceProportion{rp2:.4f}-holdingPeriod{hp:.0f}d-lever{lv:.0f}-multiAssetPick{map}'.format(
				rp1=self.__rebalancePeriod, rp2=self.__rebalanceProportion, hp=self.__holdingPeriod, lv=self.__leverage, map=self.__multiAssetPick)
		to_save = self.series.merge(self.historical_portfolio, left_index=True, right_index=True)
		to_save.to_csv(path + name + '.csv')
		print("Saved file as {}".format(path + name + '.csv'))
		return to_save



def main():
	data = pd.read_csv('C:\\Users\\33695\\Desktop\\QIS_Data\\OutputSlideDayTargetToPredictWhatToPickValidationToBacktest.csv', index_col=0)

	backtester = assetBasket(data)
	bt = backtester.backtest(holdingPeriod=21, rebalancePeriod=1, multiAssetPick=True, leverage=1)
	backtester.save(path='C:\\Users\\33695\\Desktop\\QIS_Data\\')
	print(bt)





if __name__ == '__main__':
    main()

