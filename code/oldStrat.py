import pandas as pd
import yfinance as yf
import numpy as np

#We create a function to leverage the log returns
def leveraged_asset_logreturns(asset_logreturns, rate, leverage=1):
    temp = pd.DataFrame(rate)
    temp['tvalue'] = temp.index
    temp = (temp['tvalue'] - temp['tvalue'].shift()).dt.days
    return np.log(((np.exp(asset_logreturns) - 1)* leverage - (rate * temp/365) * (leverage - 1))+1)


#We create a function for the rolling decision rule
def decision_rule(asset, days):
    return np.sign((asset/asset.shift(days) - 1).fillna(0)).shift(1, fill_value=0)


#And a function to compute our strategy
def strategy(asset1, asset2, rate, leverage, days):
    decision = decision_rule(asset1, days)
    asset1_lr = np.log(asset1).diff()
    asset2_lr = np.log(asset2).diff()
    asset1_leveraged = leveraged_asset_logreturns(asset1_lr, rate, leverage=leverage)
    
    strat_lr = np.maximum(decision, 0) * asset1_leveraged -  np.minimum(decision, 0) * asset2_lr
    
    strategy = 100 * np.exp(strat_lr.cumsum())
    strategy.iloc[0,] = 100
    
    performance = np.mean(np.exp(strat_lr)-1)
    
    return strat_lr, strategy, performance




def main():
    #Obtention des données
    sp_data = yf.download('^GSPC', '2000-01-05', '2100-01-01').dropna()['Adj Close']
    eg_data = pd.read_csv('C:\\Users\\33695\\Desktop\\Python\\PortfolioAllocatorV2\\Rapport Deep Learning\\Data\\tp2.csv',index_col=0, sep=';')
    eg_data.index = pd.to_datetime(eg_data.index)
    new_data = pd.concat([eg_data, sp_data], axis=1, join="inner").rename(columns={'Adj Close':'S&P500'})

    new_data['Eonia'] = new_data['Eonia'] + 0.02

    #We compute it for usual values
    lr, strat, perf = strategy(new_data['S&P500'], new_data['LBMA Gold'], new_data['Eonia'], 2, 252)

    strat.to_csv('C:\\Users\\33695\\Desktop\\Python\\PortfolioAllocatorV2\\Rapport Deep Learning\\oldStrat.csv')


if __name__ == '__main__':
    main()