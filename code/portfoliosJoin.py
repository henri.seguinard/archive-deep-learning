import numpy as np
import pandas as pd
import yfinance as yf
import backtestBasket as bb



def loadPorfolios(pathToPortfolios):
	portfolios = pd.read_csv(pathToPortfolios, index_col=0)
	portfolios.index = pd.to_datetime(portfolios.index, format='%d/%m/%Y')
	return portfolios

def downloadAndAggregateData(tickerList, start, end):
	data = yf.download(tickerList, start, end)['Adj Close']
	data = data.reindex(columns=tickerList)
	return data

def mergeData(porfolios, ticks):
	output = ticks.join(porfolios)
	output = output.fillna(method='ffill')
	return output

def portfolioToBacktest(pathToPortfolios, tickerList):
	portfolios = loadPorfolios(pathToPortfolios)
	start = '2016-10-01'
	end = None
	ticks = downloadAndAggregateData(tickerList, start, end)
	new_data = mergeData(portfolios, ticks)
	return new_data





def main():
	portfolio = portfolioToBacktest('porfolios100NN.csv', ['SSO', 'SGOL'])
	backtest = bb.assetBasket(
		portfolio, 
		nulPickWeight=True,
		weightsComputedAtEOD=True)
	bt = backtest.backtest(
		rebalancePeriod=1, 
		rebalanceProportion=1, 
		holdingPeriod=1, 
		multiAssetPick=False)
	backtest.save(path='backtest100NN')
	import matplotlib.pyplot  as plt
	bt['backtest'].plot()
	plt.show()
	


if __name__ == '__main__':
	main()