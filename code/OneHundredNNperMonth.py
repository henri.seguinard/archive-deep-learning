import pandas as pd
import numpy as np
import datetime
import calendar
import csv

import financialTimeSeriesAnalytics as fta
import downloadSeriesAndGenerateAnalytics as dsaga
import kerasClassificationFromGeneratedAnalytcis as ker
import backtestBasket as bb

pd.set_option('display.max_rows', 100)


class NeuralNetworkAssetAllocation():
	def __init__(self):
		return
#I - Generate or Load Analytics
	def loadAnalytics(self, pathToAnalytics):
		analytics = pd.read_csv(pathToAnalytics, index_col=0)
		analytics.index = pd.to_datetime(analytics.index, format='%d/%m/%Y')
		self.dataSet = analytics
		self.nAsset = None
		return analytics
#II - Build and train 
#II - a) Create modelDates matrix
	def __check_if_last_day_of_month(self, date):
		last_day_of_month = calendar.monthrange(date.year, date.month)[1]
		if date == datetime.date(date.year, date.month, last_day_of_month):
			return True
		return False
	def __create_last_day_of_month_mask(self, dateList):
		lastDayOfMonthMask=list()
		indexOfLastDays=list()
		for i in range(len(dateList)-1):
			if dateList[i].replace(day=1) != dateList[i+1].replace(day=1):
				lastDayOfMonthMask.append(True)
				indexOfLastDays.append(i)
			else:
				lastDayOfMonthMask.append(False)
		lastDayOfDataset = self.__check_if_last_day_of_month(dateList[-1])
		lastDayOfMonthMask.append(lastDayOfDataset)
		if lastDayOfDataset:
			indexOfLastDays.append(len(dateList) - 1)
		return lastDayOfMonthMask, indexOfLastDays
	def __generateModelDates(self, trainingAndValidationSetLength):
		#Get last day of each month
		dates = self.dataSet.index.to_list()
		lastDayMask , lastDayIndex = self.__create_last_day_of_month_mask(dates)
		firstDayIndex = [x + 1 - trainingAndValidationSetLength for x in lastDayIndex]
		firstDate = [dates[x] for x in firstDayIndex]
		lastDate = [dates[x] for x in lastDayIndex]
		#Construct the matrix and remove impossible dates
		modelDates = pd.DataFrame(
			{
			'firstDate':firstDate,
			'lastDate':lastDate,
			'firstDateIndex':firstDayIndex, 
			'lastDateIndex':lastDayIndex 
			})
		modelDates =  modelDates[modelDates['firstDateIndex'] > 0].reset_index(drop=True)
		#Return result
		self.modelDates = modelDates
		return modelDates
#II - a) Generate models
	def __check_for_nAsset(self, nAsset):
		if self.nAsset is None and nAsset is None:
			raise AttributeError("If you used self.loadAnalytics() you need to specify nAsset here")
		elif self.nAsset is None:
			self.nAsset = nAsset
		return
	def __prepare_dataSet_for_ker(self):
		df = self.dataSet
		temp =  df.index
		df.reset_index(drop=True, inplace=True)
		np.asarray(df).astype('float32')
		df.insert(0, 'Date',temp)
		self.dataSet = df
		return
	def __split_train_test_validation(self, dataSubSet, trainValidationSplit):
		trainSet = dataSubSet[:int(len(dataSubSet)*trainValidationSplit)]
		validationSet = dataSubSet[int(len(dataSubSet)*trainValidationSplit):]
		#Only keep train set data starting from row where we have all data
		trainSet = trainSet.dropna()
		return trainSet, validationSet
	def __generate_individual_model(self, dataSubSetTrain, nAsset,
			nEpoch, nDenseDropoutLayer, dropoutRate, nWidthDenseLayer):
		model = ker.neuralNetwork(
			nAsset,
			nEpoch=nEpoch, 
			nDenseDropoutLayer=nDenseDropoutLayer, 
			dropoutRate=dropoutRate, 
			nWidthDenseLayer=nWidthDenseLayer)
		model.build(dataSubSetTrain)
		return model
	def __assess_backtest(self, backtest):
		timeSeries = backtest['backtest'].to_numpy()
		dates = np.atleast_1d(np.asarray(backtest.index.array))
		timeSeries = fta.financialTimeSeries(timeSeries, 
			entryType='timeSeries',
			entryDates=dates)
		timeSeries.basic()
		timeSeries.distribution()
		timeSeries.drawDown()
		perf = (timeSeries.basic.sharpe() 
			* (1 + timeSeries.distribution.skewness())
			* (1 + timeSeries.drawDown.max()))
		return perf
	def __validate_individual_model(self, model, dataSubSetValidation,
		rebalancePeriod, rebalanceProportion, holdingPeriod):
		toBacktest = model.validate(dataSubSetValidation)
		backtest = bb.assetBasket(toBacktest)
		bt = backtest.backtest(
			rebalancePeriod=rebalancePeriod, 
			rebalanceProportion=rebalanceProportion, 
			holdingPeriod=holdingPeriod)
		assesment = self.__assess_backtest(bt)
		currentPortfolio = backtest.historical_portfolio.tail(1)
		return assesment, currentPortfolio
	def __generate_and_compare_models(self, 
			dataSubSetTrain, dataSubSetValidation,
			nAsset,
			nEpoch, nDenseDropoutLayer, dropoutRate, nWidthDenseLayer,
			rebalancePeriod, rebalanceProportion, holdingPeriod,
			nModel, bestModel):
		modelList = list()
		modelPerformance = list()
		modelPortfolio = list()
		for i in range(nModel):
			print('Generating model {}/{}'.format(i+1, nModel))
			model = self.__generate_individual_model(
				dataSubSetTrain,
				nAsset,
				nEpoch, 
				nDenseDropoutLayer, 
				dropoutRate, 
				nWidthDenseLayer)
			assessment, currentPortfolio = self.__validate_individual_model(
				model, 
				dataSubSetValidation,
				rebalancePeriod, 
				rebalanceProportion, 
				holdingPeriod)
			modelList.append(model)
			modelPerformance.append(assessment)
			modelPortfolio.append(currentPortfolio)
		bestPerformanceIndex = sorted(range(len(modelPerformance)), key = lambda sub: modelPerformance[sub])[-bestModel:]
		bestModels =  [modelList[i] for i in bestPerformanceIndex]
		bestPortfolios = [modelPortfolio[i] for i in bestPerformanceIndex]
		bestPerformance = [modelPerformance[i] for i in bestPerformanceIndex]
		bestPortfolios = np.mean(pd.concat(bestPortfolios), axis=0)
		bestPortfolios = pd.DataFrame(bestPortfolios).transpose()
		bestPortfolios = bestPortfolios.set_index(modelPortfolio[0].index)
		return bestPortfolios
	def generatePortfolios(self,
		nAsset=None,
		trainingAndValidationSetLength=252*6,
		trainValidationSplit=2/3,
		nEpoch=80, 
		nDenseDropoutLayer=0, 
		dropoutRate=0.2, 
		nWidthDenseLayer=100,
		rebalancePeriod=21, 
		rebalanceProportion=None, 
		holdingPeriod=21,
		nModel=25, 
		bestModel=5,
		save=True,
		path="",
		name="porfolios100NN"):
		self.__check_for_nAsset(nAsset)
		self.__generateModelDates(trainingAndValidationSetLength)
		self.__prepare_dataSet_for_ker()
		portfolioList = list()
		for i in range(16, len(self.modelDates)):
			print('\n')
			print('-'*50)
			print('Generating for dates {}/{}'.format(i+1, len(self.modelDates)))
			print('-'*50)
			beg = self.modelDates.iloc[i]['firstDateIndex']
			end = self.modelDates.iloc[i]['lastDateIndex']
			dataSubSet = self.dataSet.iloc[beg:end+1,:]
			dataSubSetTrain, dataSubSetValidation = self.__split_train_test_validation(
				dataSubSet, 
				trainValidationSplit)
			bestPortfolios = self.__generate_and_compare_models(
				dataSubSetTrain, 
				dataSubSetValidation,
				nAsset,
				nEpoch, 
				nDenseDropoutLayer, 
				dropoutRate, 
				nWidthDenseLayer,
				rebalancePeriod, 
				rebalanceProportion, 
				holdingPeriod,
				nModel, 
				bestModel)
			portfolioList.append(bestPortfolios)
			if i == 0:
				bestPortfolios.to_csv(path + name + 'temporary.csv')
			else:
				bestPortfolios.to_csv(path + name + 'temporary.csv', mode='a', index=True, header=False)
		portfolioList = pd.concat(portfolioList)
		if save:
			portfolioList.to_csv(path + name + '.csv')
		self.portfolios = portfolioList
		return portfolioList






def main():
	#I - Generate data
	#dsaga.main()
	#II - Train models and generate portfolio
	pathToAnalytics = 'C:\\Users\\33695\\Desktop\\QIS_Data\\100NeuralNetworksPerMonths.csv'
	a= NeuralNetworkAssetAllocation()
	a.loadAnalytics(pathToAnalytics)
	a.generatePortfolios(nAsset=3)


if __name__ == '__main__':
	main()


